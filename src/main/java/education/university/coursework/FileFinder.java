package education.university.coursework;

import org.apache.poi.hwpf.HWPFDocument;
import org.apache.poi.hwpf.extractor.WordExtractor;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.apache.poi.xwpf.usermodel.XWPFParagraph;
import sun.awt.image.ImageWatched;

import java.io.*;
import java.util.*;

/**
 * Created by Robert on 06.12.2015.
 */
public class FileFinder {
   private static LinkedList<File> filesFound = new LinkedList<>();


    static LinkedList<File> listOfFiles(String szDir,String textToSearch) {
     //   System.out.println("SEARCH START with args " + szDir + " " + textToSearch);
        Arrays.stream(Optional.ofNullable(new File(szDir).list()).orElse(new String[0])).parallel()
                .map(s -> szDir + File.separator + s)
                .map(File::new)
                .forEach(f -> {
                    if (f.isFile() && !f.getName().contains("~$")) {
                        if (f.getName().equals(textToSearch)) {
                            filesFound.add(f);
                        }
                        else{
                            String ext = getExtention(f.getName());
                            if(ext.equalsIgnoreCase("txt"))
                                readFiles(f.getAbsolutePath(),textToSearch);
                            if(ext.equalsIgnoreCase("docx"))
                                readDocxFile(f.getAbsolutePath(),textToSearch);
                            if(ext.equalsIgnoreCase("doc"))
                                readDocFile(f.getAbsolutePath(),textToSearch);
                            if(ext.equalsIgnoreCase("xlsx"))
                                readExcelFile(f.getAbsolutePath(),textToSearch);

                        }
                    } else {
                        listOfFiles(f.getAbsolutePath(), textToSearch);
                    }
                });
        return filesFound;

    }
    private static void readFiles(String dir,String textToSearch) {
        textToSearch = textToSearch.toLowerCase();
        File currentFile = new File(dir);
        try {

            BufferedReader reader = new BufferedReader(new FileReader(currentFile));
            String string;
            while ((string = reader.readLine()) != null) {
                if (string.toLowerCase().contains(textToSearch.toLowerCase())) {
                  //  System.out.println("Found TXT");
                    filesFound.add(currentFile);
                    break;
                }
            }
            reader.close();
        }catch (IOException e){
            e.printStackTrace();
        }
    }
    private static void readDocFile(String fileName,String textToSearch) {
        String FileName="";//temp
        try {
            File file = new File(fileName);
            FileInputStream fis = new FileInputStream(file);
            FileName = file.getAbsolutePath();
            HWPFDocument doc = new HWPFDocument(fis);

            WordExtractor we = new WordExtractor(doc);

            String[] paragraphs = we.getParagraphText();
            for (String para : paragraphs) {
                if(para.toLowerCase().contains(textToSearch.toLowerCase())) {
                    //System.out.println("Found Doc");
                    filesFound.add(file);
                    break;
                }
            }
            fis.close();
        } catch (Exception e) {
            e.printStackTrace();
         //   System.out.println(FileName);
        }

    }

    public static void readDocxFile(String fileName, String textToSearch) {
        String FileName="";
        try {
            File file = new File(fileName);
            FileInputStream fis = new FileInputStream(file.getAbsolutePath());
            FileName = file.getAbsolutePath();
            XWPFDocument document = new XWPFDocument(fis);

            List<XWPFParagraph> paragraphs = document.getParagraphs();

            for (XWPFParagraph para : paragraphs) {
                if(para.getText().toLowerCase().contains(textToSearch.toLowerCase())){
                  //  System.out.println("Found Docx");
                   filesFound.add(file);
                    break;
                }
            }
            fis.close();
        } catch (Exception e) {
            e.printStackTrace();
          //  System.out.println(FileName);
        }
    }
    private static void readExcelFile(String fileName, String textToSearch){
        FileInputStream fileInputStream = null;
        File file = new File(fileName);
       try {
           fileInputStream = new FileInputStream(file);

           //Create Workbook instance holding reference to .xlsx file
           XSSFWorkbook workbook = null;
           workbook = new XSSFWorkbook(fileInputStream);

           //Get first/desired sheet from the workbook
           XSSFSheet sheet = workbook.getSheetAt(0);

           //Ітерація по рядкам
           Iterator<Row> rowIterator = sheet.iterator();
           while (rowIterator.hasNext()) {
               Row row = rowIterator.next();
               //Для кожної строки ітерація по колонкам
               Iterator<Cell> cellIterator = row.cellIterator();
               String result = "";
               while (cellIterator.hasNext()) {
                   Cell cell = cellIterator.next();
                   //Check the cell type and format accordingly
                   switch (cell.getCellType()) {
                       case Cell.CELL_TYPE_NUMERIC:
                           result += cell.getNumericCellValue() + " ";
                           break;
                       case Cell.CELL_TYPE_STRING:
                           result += cell.getStringCellValue() + " ";
                           break;
                   }
                   if(result.toLowerCase().contains(textToSearch.toLowerCase())){
                       filesFound.add(file);
                       break;
                   }
               }

           }
           fileInputStream.close();
       }catch (IOException e){
           e.printStackTrace();
       }
    }

    public static String getExtention(String fileName){
        String[] parts = fileName.split("[.]");
        return parts[parts.length-1];
    }
}
